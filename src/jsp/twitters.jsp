<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html;" %>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <style>
        .container {
            padding: 80px 120px;
        }
    </style>

</head>
<body>




<jsp:include page="message.jsp"></jsp:include>

<jsp:include page="hi.jsp"/>

<jsp:include page="menu.jsp"></jsp:include>


<div class="container">
    <h1>Twitter List :</h1>
    <%--<p>To show the quote on the right use the class .blockquote-reverse:</p>--%>


    <%--<c:forEach items="${twitters}" var="twitt">--%>

    <%--<p>${twitt.message}</p>--%>
    <%--<p>Created by ${twitt.userName}  on ${twitt.dateOfCreation}</p>--%>
    <%--<p>Favourite ${twitt.favouriteTweet}--%>
    <%--<form method="post" action="/com.firstjsp/favourite_tweets">--%>
    <%--<input type="hidden" name="twittId" value="${twitt.id}"/>--%>
    <%--<input type="submit" value="Add to favourites" >--%>
    <%--</form>--%>
    <%--</p>--%>
    <%--</c:forEach>--%>




    <br>

    <c:forEach items="${twitters}" var="twitt">
    <blockquote class="blockquote">
        <p class="bg-success">


                ${twitt.message}


        </p>

        <footer>Created by ${twitt.userName} on ${twitt.dateOfCreation}
                <%--favorite twitt ${twitt.favorite}--%>


        </footer>

        <div class="row">

            <h3>
                <div class="col-sm-4">


                    <c:if test="${twitt.favorite==false}">


                        <form method="post" action="/favoriteTwitts">


                            <input type="hidden" name="twittID" value="${twitt.id}"/>
                            <button type="submit" class="btn btn-success">Add to favorite!</button>



                        </form>

                    </c:if>

                    <c:if test="${twitt.favorite==true}">


                        <form method="post" action="/removeFavoriteTwitts">


                            <input type="hidden" name="twittID" value="${twitt.id}"/>
                            <button type="submit" class="btn btn-warning">Remove from favorite :(</button>


                        </form>

                    </c:if>

                </div>

            </h3>

            <h3>
                <div class="col-sm-4">




                </div>

            </h3>
            <h3>
                <div class="col-sm-4">
                    <c:if test="${sessionScope.login=='admin'}">


                        <form method="post" action="/all">


                            <input type="hidden" name="twittID" value="${twitt.id}"/>
                            <button type="submit" class="btn btn-danger">Delete this twitt!</button>


                        </form>

                    </c:if>


                </div>

            </h3>

        </div>
    </blockquote>
    </c:forEach>


</body>


</html>
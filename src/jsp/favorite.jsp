<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html;" %>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <style>
        .container {
            padding: 80px 120px;
        }
    </style>

</head>
<body>




<div class="container text-center">


    <c:if test="${not empty sessionScope.login}">
        <h3> ${sessionScope.login} this are your favorite twitts</h3>
        <p><em>Hope you are proud :) </em></p>

    </c:if>

</div>


<jsp:include page="menu.jsp"></jsp:include>

<div class="container">
    <h1>Twitter List :</h1>
    <%--<p>To show the quote on the right use the class .blockquote-reverse:</p>--%>
    <c:forEach items="${favoriteTwitters}" var="twitt">
    <blockquote class="blockquote">
        <p class="bg-success">${twitt.message}</p>
        <footer>Created by ${twitt.userName} on ${twitt.dateOfCreation}
            <%--favorite twitt ${twitt.favorite}--%>


        </footer>
    </blockquote>
    </c:forEach>



</body>


</html
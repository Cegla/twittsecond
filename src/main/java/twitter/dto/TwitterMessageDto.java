package twitter.dto;

import java.util.Date;

public class TwitterMessageDto {


    private long id;
    private String message;
    private Date dateOfCreation;
    private String userName;
    private  boolean favorite;

    public TwitterMessageDto(){}

    public TwitterMessageDto(long id, String message, Date dateOfCreation, String userName, boolean favorite) {
        this.id = id;
        this.message = message;
        this.dateOfCreation = dateOfCreation;
        this.userName = userName;
        this.favorite = favorite;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
}


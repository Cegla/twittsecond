package twitter.controller;

import sun.rmi.server.Dispatcher;
import twitter.repository.TwitterRepository;
import twitter.repository.impl.TwitterRepositoryImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "login", value = "/login")
public class LoginControler extends HttpServlet {

    private TwitterRepository repository = new TwitterRepositoryImpl();



    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {



        RequestDispatcher dispatcher = req.getRequestDispatcher("login.jsp");
        dispatcher.forward(req, resp);


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher dispatcher = req.getRequestDispatcher("login.jsp");

        String login = (String) req.getParameter("login");
        String password = (String) req.getParameter("password");
        String servletPath = req.getParameter("backUrl");
        if (servletPath.equals("")){
            servletPath="/all";
        }



        if (repository.chekIfValidUser(login, password)) {
//        if (login.equals("admin") && password.equals("admin")) {
            HttpSession session = req.getSession();
            session.setAttribute("login",login);

            resp.sendRedirect(servletPath);


        } else {
            req.setAttribute("notValid", true);
            dispatcher.forward(req, resp);

        }


    }
}

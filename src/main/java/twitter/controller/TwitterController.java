package twitter.controller;

import twitter.cookie.CookieHelper;
import twitter.dto.TwitterMessageDto;
import twitter.model.TwitterMessage;
import twitter.service.TwitterService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static twitter.enums.TwitterEnums.TWITTS_ID_COOKIE_NAME;
import static twitter.enums.TwitterMessage.TWITT_ADDED;

/**
 * Created by Rafal on 2017-08-31.
 */

@WebServlet(name = "TwitterController", value = "/all")
public class TwitterController extends HttpServlet {

    private TwitterService service = new TwitterService();
    private ControlerUtil util = new ControlerUtil();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String userMessage =  req.getParameter("userMessage");

        if(userMessage!=null){
            if(userMessage.equals(TWITT_ADDED.getMessag())){
                req.setAttribute("userMessage",TWITT_ADDED.getMessag());
            }
        }







        String cookieValueOfFavoriteTwitts = CookieHelper.getCookieValue(req.getCookies(), TWITTS_ID_COOKIE_NAME.getEnumName());


        List<TwitterMessageDto> twitters = service.getTwitterMessage(cookieValueOfFavoriteTwitts);


        req.setAttribute("twitters", twitters);
        RequestDispatcher dispatcher = req.getRequestDispatcher("twitters.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String idOfTwittToDelete = req.getParameter("twittID");
        util.removingCookie(req,resp);
        service.deletingTwitt(idOfTwittToDelete);
//        if(idOfTwittToDelete!=null){
//            Long id = Long.parseLong(idOfTwittToDelete);
//            service.deleteTweetByID(id);
//
//        }
        resp.sendRedirect("/all");


    }


    //    private List<TwitterMessageDto> getTwitterMessage(String cookieValueOfFavoriteTwitts) {
//        List<TwitterMessageDto> twitters = service.getAllTwitts();
//        List<TwitterMessageDto> favoriteTwitters = service.getFavoriteTwitts(cookieValueOfFavoriteTwitts);
//
//
//        twitters.stream()
//                .forEach(t -> {
//                    if(favoriteTwitters.stream().anyMatch(f -> t.getId()==f.getId())){
//                        t.setFavorite(true);
//                    }
//                });
//        return twitters;
//    }


}
package twitter.controller;

import twitter.cookie.CookieHelper;
import twitter.service.TwitterService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static twitter.enums.TwitterEnums.TWITTS_ID_COOKIE_NAME;

public class ControlerUtil extends HttpServlet {

    TwitterService twitterService = new TwitterService();

    public void removingCookie(HttpServletRequest req, HttpServletResponse resp) {
        String twittId = req.getParameter(TWITTS_ID_COOKIE_NAME.getEnumName());

        Cookie[] allCookies = req.getCookies();

        String cookieValue = CookieHelper.getCookieValue(allCookies, TWITTS_ID_COOKIE_NAME.getEnumName());


        Cookie newCookie = twitterService.removeCookie(twittId, new Cookie(TWITTS_ID_COOKIE_NAME.getEnumName(), cookieValue));

        resp.addCookie(newCookie);
    }
}

package twitter.controller;


import twitter.model.User;
import twitter.repository.TwitterRepository;
import twitter.repository.impl.TwitterRepositoryImpl;
import twitter.service.TwitterService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "user", value = "/user")
public class UserControler extends HttpServlet {

    private TwitterService twitterService = new TwitterService();
    private TwitterRepository repository = new TwitterRepositoryImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("user.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("user.jsp");
        String name = req.getParameter("name");
        String surnem = req.getParameter("surname");
        String password = req.getParameter("password");
        String login = req.getParameter("login");

        if(repository.checkIfUsersAlreadyExists(new User(name,surnem,login,password))) {
                twitterService.creatTwitterUser(name, surnem, login, password);
            resp.sendRedirect("/all");

        }

        else {
            req.setAttribute("alreadyExist",true);
                        dispatcher.forward(req, resp);

        }


    }
}

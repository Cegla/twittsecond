package twitter.controller;


import twitter.cookie.CookieHelper;
import twitter.service.TwitterService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "favoritTwittRemove", value = "/removeFavoriteTwitts")
public class FavoriteTwittsRemoveControler extends HttpServlet {

    TwitterService twitterService = new TwitterService();
    ControlerUtil util = new ControlerUtil();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {



        util.removingCookie(req,resp);


        resp.sendRedirect("/all");

    }
}

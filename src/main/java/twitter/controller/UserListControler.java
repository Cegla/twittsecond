package twitter.controller;


import twitter.model.User;
import twitter.service.TwitterUserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "allUsers", value = "/users")
public class UserListControler extends HttpServlet{

    private TwitterUserService userService = new TwitterUserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<User> users = userService.getAllUsers();
        req.setAttribute("users",users);

        RequestDispatcher dispatcher = req.getRequestDispatcher("allUsers.jsp");

        dispatcher.forward(req,resp);



    }
}

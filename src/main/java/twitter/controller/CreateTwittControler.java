package twitter.controller;

import twitter.enums.ValidationErrors;
import twitter.service.TwitterService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "createTweet", value = "/create")
public class CreateTwittControler extends HttpServlet {

    private TwitterService twitterService = new TwitterService();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher dispatcher = req.getRequestDispatcher("create.jsp");
        dispatcher.forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = (String) req.getSession().getAttribute("login");
        String message = req.getParameter("message");
        twitterService.createTwitterMessage(userName, message);
        resp.sendRedirect("/all?userMessage=TWITT_ADDED");
    }


}

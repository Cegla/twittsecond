package twitter.controller;

import twitter.cookie.CookieHelper;
import twitter.dto.TwitterMessageDto;
import twitter.repository.TwitterRepository;
import twitter.repository.impl.TwitterRepositoryImpl;
import twitter.service.TwitterService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static twitter.enums.TwitterEnums.TWITTS_ID_COOKIE_NAME;

@WebServlet(name = "favoriteTwittes", value = "/favoriteTwitts")
public class FavoriteTwittesAddControler extends HttpServlet {


//    private static final String TWITTS_ID_COOKIE_NAME = "twittID";

    private TwitterRepository twitterRepository = new TwitterRepositoryImpl();

    private TwitterService twitterService = new TwitterService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {



        String cookieValueOfFavoriteTwitts = CookieHelper.getCookieValue(req.getCookies(), TWITTS_ID_COOKIE_NAME.getEnumName());
        List<TwitterMessageDto> favoriteTwitts = twitterService.getFavoriteTwitts(cookieValueOfFavoriteTwitts);



        req.setAttribute("favoriteTwitters", favoriteTwitts);
        RequestDispatcher dispatcher = req.getRequestDispatcher("favorite.jsp");
        dispatcher.forward(req, resp);


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String twittId = req.getParameter(TWITTS_ID_COOKIE_NAME.getEnumName());

        Cookie[] allCookies = req.getCookies();

        String cookieValue=CookieHelper.getCookieValue(allCookies, TWITTS_ID_COOKIE_NAME.getEnumName());


        Cookie newCookie = twitterService.SetCookie(twittId, new Cookie(TWITTS_ID_COOKIE_NAME.getEnumName(),cookieValue));
        resp.addCookie(newCookie);
        resp.sendRedirect("/all");




    }



    }

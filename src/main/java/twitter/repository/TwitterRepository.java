package twitter.repository;

import twitter.dto.TwitterMessageDto;
import twitter.model.TwitterMessage;
import twitter.model.User;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Rafal on 2017-09-01.
 */
public interface TwitterRepository {



    List<TwitterMessageDto> getAllTwitts();

    List<User> getAllUsers();

    TwitterMessage persistTwitterMessage(TwitterMessage twitterMessage);

    TwitterMessage getTwittById(long id);

    void deleteTwittByID(long id);

    User presistUser(User user);


    boolean chekIfValidUser(String login, String password);

    boolean checkIfUsersAlreadyExists(User user);




}

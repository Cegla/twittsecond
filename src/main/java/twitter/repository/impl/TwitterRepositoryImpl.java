package twitter.repository.impl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import twitter.dto.TwitterMessageDto;
import twitter.mapper.TwitterDtoMapper;
import twitter.mapper.TwitterDtoMapperImpl;
import twitter.model.TwitterMessage;
import twitter.model.User;
import twitter.repository.TwitterRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Rafal on 2017-08-31.
 */
public class TwitterRepositoryImpl implements TwitterRepository {

    private SessionFactory sessionFactory;

    private TwitterDtoMapper twitterDtoMapper = new TwitterDtoMapperImpl();



    public TwitterRepositoryImpl() {
        initRepo();
    }

    private void initRepo() {
        Configuration config = new Configuration().configure();
        config.addAnnotatedClass(TwitterMessage.class);
        config.addAnnotatedClass(User.class);
        ServiceRegistry service = new ServiceRegistryBuilder().applySettings(config.getProperties()).buildServiceRegistry();
        sessionFactory = config.buildSessionFactory(service);
//        test();
    }

    private void test() {
        Session session = sessionFactory.openSession();

        Calendar startDate = new GregorianCalendar(2017, 8, 11, 8, 0, 0);
        TwitterMessage m = new TwitterMessage("First twitt", startDate.getTime(), "Ziomeczke");
        session.beginTransaction();
        session.save(m);
        session.getTransaction().commit();
    }



    public List<TwitterMessageDto> getAllTwitts() {
        Session session = sessionFactory.openSession();
        List<TwitterMessage> twitters = session.createQuery("from TwitterMessage").list();
        List<TwitterMessageDto> twitterMessageDtos = new ArrayList<TwitterMessageDto>();
        if(!twitters.isEmpty())
        for (TwitterMessage twitt: twitters)
        {
           twitterMessageDtos.add(twitterDtoMapper.map(twitt));

        }
        return twitterMessageDtos;
    }

    public List<User> getAllUsers() {
        Session session = sessionFactory.openSession();

        List<User> users = session.createCriteria(User.class).list();
//    List<User> users = session.createQuery("from Users").list();
        return users;

    }

    public TwitterMessage persistTwitterMessage(TwitterMessage twitterMessage) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(twitterMessage);
        session.getTransaction().commit();

        return twitterMessage;

    }

    public User presistUser(User user) {
        Session session = sessionFactory.openSession();
//        if (checkIfUsersAlreadyExists(session, user)) {
        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();
        return user;
//        } else {
//            throw new EntityExistsException(String.format("User %s %s already exists", user.getName(), user.getName()));
//        }


    }


    public boolean chekIfValidUser(String login, String password) {
        Session session = sessionFactory.openSession();

        Criteria criteriaSurname = session.createCriteria(User.class);
        criteriaSurname.add(Restrictions.eq("login", login));
        Criteria criteriaName = criteriaSurname.add(Restrictions.eq("password", password));

        return !criteriaName.list().isEmpty();


    }

    public boolean checkIfUsersAlreadyExists(User user) {
        Session session = sessionFactory.openSession();

        Criteria criteriaSurname = session.createCriteria(User.class);
        criteriaSurname.add(Restrictions.eq("surname", user.getSurname()));
        Criteria criteriaName = criteriaSurname.add(Restrictions.eq("name", user.getName()));
        return criteriaName.list().isEmpty();
    }



    @Override
    public TwitterMessage getTwittById(long id) {
        Session session = sessionFactory.openSession();
        TwitterMessage twitterMessage = (TwitterMessage) session.load(TwitterMessage.class, id);

        return twitterMessage;
    }

    @Override
    public void deleteTwittByID(long id) {


        Session session = sessionFactory.openSession();
        TwitterMessage twitterMessage = (TwitterMessage) session.load(TwitterMessage.class, id);
        session.beginTransaction();
        session.delete(twitterMessage);
        session.getTransaction().commit();


    }


}




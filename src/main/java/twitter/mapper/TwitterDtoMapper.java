package twitter.mapper;

import twitter.dto.TwitterMessageDto;
import twitter.model.TwitterMessage;

public interface TwitterDtoMapper {

    TwitterMessageDto map(TwitterMessage twitterMessage);

}

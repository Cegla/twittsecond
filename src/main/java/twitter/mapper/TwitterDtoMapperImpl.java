package twitter.mapper;

import twitter.dto.TwitterMessageDto;
import twitter.model.TwitterMessage;

public class TwitterDtoMapperImpl implements TwitterDtoMapper {

    public TwitterMessageDto map(TwitterMessage twitterMessage) {

        TwitterMessageDto twitterMessageDto = new TwitterMessageDto();

        twitterMessageDto.setId(twitterMessage.getId());
        twitterMessageDto.setDateOfCreation(twitterMessage.getDateOfCreation());
        twitterMessageDto.setMessage(twitterMessage.getMessage());
        twitterMessageDto.setUserName(twitterMessage.getUserName());
        twitterMessageDto.setFavorite(false);
        return twitterMessageDto;
    }
}

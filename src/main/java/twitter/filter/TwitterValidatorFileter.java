package twitter.filter;

import twitter.enums.ValidationErrors;
import twitter.validator.TwitterMessageValidator;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static twitter.enums.TwitterEnums.TWITTS_ID_COOKIE_NAME;
import static twitter.enums.ValidationErrors.EMPTY_USER_NAME;
import static twitter.enums.ValidationErrors.TWITTER_MESSAGE;


@WebFilter("/create")
public class TwitterValidatorFileter implements Filter {



    private TwitterMessageValidator messageValidator = new TwitterMessageValidator();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        if (httpServletRequest.getMethod().equals("POST")) {


            String message = httpServletRequest.getParameter("message");


               if (message != null && message.equals("")) {
                   String newMessage = "Ale zajebisty Twitter";
                   httpServletRequest.setAttribute("error",TWITTER_MESSAGE.getMessage());
                   httpServletRequest.setAttribute("message",newMessage);
                   RequestDispatcher dispatcher = httpServletRequest.getRequestDispatcher("create.jsp");
                   dispatcher.forward(httpServletRequest,httpServletResponse);
                   return;
        }







            }
        // pass the request along the filter chain
        filterChain.doFilter(servletRequest,servletResponse);


    }




    @Override
    public void destroy() {

    }
}


//    HttpServletResponse httpServletResponse = (HttpServletResponse)response;
//    String userName = httpServletRequest.getParameter("userName");
//    String message = httpServletRequest.getParameter("message");
//
//
//
//    List<String> errors = twitterMessageValidator.validate(userName, message);
//
//	        if (errors!=null && !errors.isEmpty()) {
//                    request.setAttribute("errors", errors);
//                    RequestDispatcher dispatcher = httpServletRequest.getRequestDispatcher("create.jsp");
//                    dispatcher.forward(httpServletRequest, httpServletResponse);
//                    return;
//                    }

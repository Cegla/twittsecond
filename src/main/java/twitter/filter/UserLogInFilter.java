package twitter.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;



@WebFilter({"/all","/create","/users", "/favoriteTwitts"})
public class UserLogInFilter implements Filter {


    public void init(FilterConfig filterConfig) throws ServletException {



    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {




        boolean logged = false;

        HttpSession session=((HttpServletRequest)request).getSession();

        if (session.getAttribute("login")!=null) {
            logged = true;
        }

        if (!logged) {
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            httpServletResponse.sendRedirect("/login?backUrl="+((HttpServletRequest) request).getServletPath());
        } else {
            // pass the request along the filter chain
            chain.doFilter(request, response);
        }


    }

    public void destroy() {

    }
}

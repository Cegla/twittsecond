package twitter.service;

import twitter.dto.TwitterMessageDto;
import twitter.mapper.TwitterDtoMapperImpl;
import twitter.model.TwitterMessage;
import twitter.model.User;
import twitter.repository.TwitterRepository;
import twitter.repository.impl.TwitterRepositoryImpl;

import javax.servlet.http.Cookie;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static twitter.enums.TwitterEnums.NO_COOKIE;
import static twitter.enums.TwitterEnums.TWITTS_ID_COOKIE_NAME;

/**
 * Created by Rafal on 2017-08-31.
 */
public class TwitterService {

    private TwitterRepository twitterRepositoryImpl = new TwitterRepositoryImpl();
    private TwitterDtoMapperImpl twitterDtoMapper = new TwitterDtoMapperImpl();

    public List<TwitterMessageDto> getAllTwitts() {
        return twitterRepositoryImpl.getAllTwitts();
    }


    public void createTwitterMessage(String userName, String message) {
        TwitterMessage twitterMessage = new TwitterMessage(message, new Date(), userName);
        twitterRepositoryImpl.persistTwitterMessage(twitterMessage);


    }

    public void creatTwitterUser(String name, String surname, String login, String password) {
        User user = new User(name, surname, login, password);

        twitterRepositoryImpl.presistUser(user);
    }

    public List<TwitterMessageDto> getFavoriteTwitts(String allCookiesValues) {


        List<TwitterMessageDto> favoriteTwitts = new ArrayList<>();

        if (allCookiesValues != null) {
            String[] arrayOfCoookieValues = allCookiesValues.split("#");
            for (String cookitValue : arrayOfCoookieValues) {
                if (cookitValue != "") {
                    Long id = Long.parseLong(cookitValue);
                    TwitterMessage twitterMessage = twitterRepositoryImpl.getTwittById(id);
                    TwitterMessageDto twitterMessageDto = twitterDtoMapper.map(twitterMessage);
                    twitterMessageDto.setFavorite(true);
                    favoriteTwitts.add(twitterMessageDto);
                }

            }
        }


        return favoriteTwitts;
    }


    public TwitterMessageDto getTwittById(Long id) {
        TwitterMessageDto twitterMessageDto = twitterDtoMapper.map(twitterRepositoryImpl.getTwittById(id));

        return twitterMessageDto;
    }

    public Cookie removeCookie(String twittId, Cookie cookie) {
        String cookieValue = cookie.getValue();

        if (!cookieValue.contains("#")) {
            cookieValue = "";
        }

        if (cookieValue != null) {
            cookieValue = cookieValue.replace(twittId + "#", "");
        }

        Cookie newCookie = new Cookie(TWITTS_ID_COOKIE_NAME.getEnumName(), cookieValue);
        newCookie.setMaxAge(60 * 60 * 24);

        return newCookie;

    }


    public Cookie SetCookie(String twittId, Cookie cookie) {

        String cookieValue = cookie.getValue();





        if (cookieValue == "" || cookieValue.equals(NO_COOKIE.getEnumName())) {
            Cookie newCookie = new Cookie(TWITTS_ID_COOKIE_NAME.getEnumName(), twittId);
            newCookie.setMaxAge(60 * 60 * 24);

            return newCookie;
        }






        if (cookieValue != null) {
            cookieValue += "#" + twittId;

            Cookie newCookie = new Cookie(TWITTS_ID_COOKIE_NAME.getEnumName(), cookieValue);
            newCookie.setMaxAge(60 * 60 * 24);

            return newCookie;
        }


        return cookie;
    }

    public void deletingTwitt(String idOfTwittToDelete) {
        if (idOfTwittToDelete != null) {
            Long id = Long.parseLong(idOfTwittToDelete);
            deleteTweetByID(id);

        }
    }


    public void deleteTweetByID(long id) {
        twitterRepositoryImpl.deleteTwittByID(id);
    }

    public List<TwitterMessageDto> getTwitterMessage(String cookieValueOfFavoriteTwitts) {
        List<TwitterMessageDto> twitters = getAllTwitts();
        if(!cookieValueOfFavoriteTwitts.equals(NO_COOKIE.getEnumName())) {
            List<TwitterMessageDto> favoriteTwitters = getFavoriteTwitts(cookieValueOfFavoriteTwitts);

            if (!favoriteTwitters.isEmpty()) {
                twitters.stream()
                        .forEach(t -> {
                            if (favoriteTwitters.stream().anyMatch(f -> t.getId() == f.getId())) {
                                t.setFavorite(true);
                            }
                        });
            }
        }
        return twitters;
    }

}

package twitter.service;

import twitter.model.User;
import twitter.repository.TwitterRepository;
import twitter.repository.impl.TwitterRepositoryImpl;

import java.util.List;

public class TwitterUserService {


    private TwitterRepository repository = new TwitterRepositoryImpl();

    public List<User> getAllUsers(){

        return repository.getAllUsers();
    }
}

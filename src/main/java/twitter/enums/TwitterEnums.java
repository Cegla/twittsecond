package twitter.enums;

public enum TwitterEnums {

    TWITTS_ID_COOKIE_NAME("twittID"), NO_COOKIE("noCookie");

    private String enumName;

    public String getEnumName(){
        return enumName;
    }

    TwitterEnums(String enumName) {
        this.enumName = enumName;
    }
}

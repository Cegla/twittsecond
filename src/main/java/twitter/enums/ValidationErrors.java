package twitter.enums;

public enum ValidationErrors {

    TWITTER_MESSAGE("Incorect twitter message"), EMPTY_USER_NAME("User name should not be empty mate :)");

    private String message;

    ValidationErrors(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

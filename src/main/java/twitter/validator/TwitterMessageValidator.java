package twitter.validator;

import java.util.ArrayList;
import java.util.List;

import static twitter.enums.ValidationErrors.EMPTY_USER_NAME;
import static twitter.enums.ValidationErrors.TWITTER_MESSAGE;

public class TwitterMessageValidator {

    public List<String> validate(String userName, String message) {
        List<String> errors = new ArrayList<>();


        if (userName != null && !userName.equals("")) {
            errors.add(EMPTY_USER_NAME.getMessage());
        }

        if (message != null && !message.equals("")) {
            errors.add(TWITTER_MESSAGE.getMessage());
        }
        return errors;
    }
}

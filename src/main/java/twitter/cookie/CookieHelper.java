package twitter.cookie;

import javax.servlet.http.Cookie;

import static twitter.enums.TwitterEnums.NO_COOKIE;

public class CookieHelper {


    public static String getCookieValue(Cookie[] cookies, String CookieName){



        if (cookies!=null) {
            for (Cookie cookieItem : cookies) {
                if (cookieItem.getName().equals(CookieName)) {
                    return cookieItem.getValue();
                }

            }
        }

        return NO_COOKIE.getEnumName();
    }


}

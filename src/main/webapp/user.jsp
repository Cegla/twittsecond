<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
    <%@ page language="java" contentType="text/html;" %>

    <html>
    <head>


        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>



    <div class="alert alert-success">

        <div class="container">
            <h2><strong>Create user</strong></h2>
            <form method="post" action="/user">
                <div class="form-group">
                    <p>What is your name ?</p>
                    <input type="text" class="form-control" id="email" placeholder="Baron" name="name">
                </div>
                <div class="form-group">
                    <p>What is your surname?</p>
                    <input type="text" class="form-control" id="pwd" placeholder="Munchausen" name="surname">
                </div>
                <div class="form-group">
                    <p>Your perfect login: </p>
                    <input type="text" class="form-control" id="pwd" placeholder="PromiscousPeeter11" name="login">
                </div>
                <div class="form-group">
                <p>Please choose top-sicret password</p>
                <input type="text" class="form-control" id="pwd" placeholder="Will forget it any way" name="password">
            </div>


                <button type="submit" class="btn btn-default">Let there be light!</button>
            </form>
        </div>


    </div>


    <%

        if(request.getAttribute("alreadyExist")!=null){
            out.print("<pre>");
            if(request.getAttribute("alreadyExist").equals(true)) {
                out.print("User already exists");
            }
            out.print("</pre>");
        }%>

    </body>


    </html>
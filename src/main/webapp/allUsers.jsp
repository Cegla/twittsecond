<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html;" %>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>




<div class="container">
    <h2>Users</h2>
    <%--<p>The .table class adds basic styling (light padding and only horizontal dividers) to a table:</p>--%>
    <table class="table">
        <thead>
        <tr>
            <th>name</th>
            <th>surname</th>
            <th>login</th>
            <th>password</th>

        </tr>
        </thead>
                <tbody>
                <c:forEach items="${users}" var="user">

                <tr>
                <th>${user.name} </th>
                <th>${user.surname} </th>
                <th>${user.login} </th>
                <th>${user.password} </th>

            </tr>
                </c:forEach>

                </tbody>



    </table>
</div>




</body>


</html>